// Shorthand for $( document ).ready()
$(function() {

    // ============== API DATA STRUCTURE ==================
    // var api_data = {
    //   base_url: "https://newsapi.org/v2/",
    //   endpoint: ["everything", "top-headlines", "sources"],
    //   search_type: ["q="],
    //   language: "language=en",
    //   sort_by: ["popularity", "relevancy", "publishedAt"],
    //   apikey: "apiKey=c95ff2e9cb53435e8f413ef505111d78"
    // };
    // ====================================================

    // ====================================================
    // build a convenience function to render each article
    var element = document.getElementById("blogs");
    element.innerHTML = "";
    function render_article(article) {
     
        element.innerHTML += "<div class=\"blog-post\">" + 
                            "<img class=\"img-responsive\" src=\"" + article['urlToImage'] + "\" height=\"400\">" +
                            "<a href=\"" + article['url'] + "\">" + 
                           "<h2 class=\"blog-post-title text-dark\">" + article['title'] + "</h2>" +
                            "<h5 class=\"blog-post-info text-muted\">" + article['description'] + "</h5>" + 
                            "</a>" +
                            "<p class=\"blog-post-meta\"> Source: " +
                            "<a href=\"" + article['source']['name'] + "\">" + article['source']['name'] + "</a>" +
                            " published at " + article['publishedAt'] + "</p>" +
                            "</div>";
    }
    // ====================================================

    // ====================================================
    // Construct your search_url
    // ... YOUR CODE GOES HERE ...
    var search_url = 'https://newsapi.org/v2/everything?' +
      'q=' + "Apple" + '&' + 'language=en&' +
      'pageSize=10&' +
      'sortBy=popularity&' +
      'apiKey=c95ff2e9cb53435e8f413ef505111d78';

    // ====================================================

    // ====================================================
    // Perform the AJAX operation
    $("button").click(function () {
      var input = $('#input').val();

      var sortBy = $("#sort").val();
      // if(sortBy === "popularity" || sortBy === "relevancy" || sortBy === "pushlishedAt") {
      //   return;
      // } else {
      //   alert("Incorrect sortby! Must be either: 'popularity', 'relevancy', 'publishedAt'");
      //   $('#sort').attr("value", "");
      //   return;
      // }

      var numOfArt = $('#numOfArt').val();
      if(isNaN(numOfArt) || numOfArt < 1 || numOfArt > 100) {
        alert("Out of range or invalid input");
        $('#numOfArt').attr("value", "");
        return;
      }

      var everything = $("#endpoint").val();

      // search_url =
      //   'https://newsapi.org/v2/everything?' +
      //   'q=' + input + '&' + 'language=en&' +
      //   'pageSize=' + numOfArt + '& sortBy=popularity&' +
      //   'apiKey=cd081ac64b6044c6ba98bf6f807c7140';
      search_url =
        'https://newsapi.org/v2/' + everything + '?' +
        'q=' + input + '&' + 'language=en&' +
        'pageSize=' + numOfArt + '& sortBy=' + sortBy + '&' +
        'apiKey=cd081ac64b6044c6ba98bf6f807c7140';

      $.ajax({
        url: search_url,
        dataType: "json",
        success: function(data) {
        // Get the total number of articles
        var n = data["totalResults"];
  
        // Get the array of articles
        var articles = data["articles"];
  
        console.log("Total Articles Found:" + n);
  
        articles.forEach(function(article) {
          // Process all of the articles on this page
          render_article(article);
        });
      },
      error: function(e) {
        console.log("AJAX Error: " + e);
      }
    });
  });
    // ====================================================
});
  